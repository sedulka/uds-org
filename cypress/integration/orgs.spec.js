//@ts-check

describe('Organisations page testing', function () {
    beforeEach(() => {
        cy.visit('/');
        cy.viewport(1400, 800)
    })

    it('just works', function () {
    })

    it('has navbar', () => {
        cy.get('[data-cy="navbar"]').should('have.length', 1)
    })

    it('has title', () => {
        cy.contains('h1', 'Учреждения досуга и спорта в Москве')
    })

    it('has switch button', () => {
        cy.get('[data-cy="switch-button"]').should('have.length', 1)
    })

    it('has footer', () => {
        cy.get('[data-cy="footer"]').should('have.length', 1)
    })

    it('should make organisations visible by clicking to area span', () => {
        cy.get('[data-cy="area"]').should('have.length.greaterThan', 0)

        cy.get('[data-cy="orgs-block"]').should('be.hidden')

        cy.get('[data-cy="area"]').click({ multiple: true, timeout: 36000 })

        cy.get('[data-cy="orgs-block"]').should('be.visible')

        cy.get('[data-cy="area"]').click({ multiple: true, timeout: 36000 })

        cy.get('[data-cy="orgs-block"]').should('be.hidden')
    })

    it('switch button should change page', () => {
        // Left button clicks
        cy.get('[data-cy="switch-button-left"]')
            .should('have.length', 1)
            .click()

        cy.get('[data-cy="area"]').should('have.length', 0)

        cy.get('[data-cy="switch-button-left"]')
            .should('have.length', 1)
            .click()

        // Right button clicks
        cy.get('[data-cy="switch-button-right"]')
            .click()

        cy.get('[data-cy="area"]')
            .should('have.length', 0)

        cy.get('[data-cy="switch-button-right"]')
            .should('have.length', 1)
            .click()
    })

    it('on small screens icon button should open navbar', () => {
        cy.viewport(800, 500)

        cy.get('a>span').should('be.hidden')

        // Get second img in DOM
        cy.get('img').eq(2).click()

        cy.get('a>span').should('be.visible')

        cy.get('img').first().click()

        cy.get('a>span').should('be.hidden')
    })

});