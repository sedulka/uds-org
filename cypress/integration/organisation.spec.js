//@ts-check

describe('OrgPage testing', () => {
    beforeEach(() => {
        cy.visit('/orgs/1');
        cy.viewport(1400, 800)
    })

    it('just works', function () { })

    it('has title', () => {
        cy.contains('h1', 'ГБУК ЦДиК «Южное Бутово»')
    })

    it('has navbar', () => {
        cy.get('[data-cy="navbar"]').should('have.length', 1)
    })

    it('has footer', () => {
        cy.get('[data-cy="footer"]').should('have.length', 1)
    })

    it('should hide and show sections after clicking twice at ages title', () => {
        // All sections should be visible at startup
        cy.get('[data-cy="section"]')
            .should('have.length.greaterThan', 0)
            .should('be.visible')

        // Close all sections
        cy.get('[data-cy="section-button"]')
            .should('have.length.greaterThan', 0)
            .click({ multiple: true })

        // Check that all sections are hidden
        cy.get('[data-cy="section"]')
            .should('have.length.greaterThan', 0)
            .should('be.hidden')

        // Open all sections
        cy.get('[data-cy="section-button"]')
            .should('have.length.greaterThan', 0)
            .click({ multiple: true })

        // Check that all sections are visible
        cy.get('[data-cy="section-button"]')
            .should('have.length.greaterThan', 0)
            .should('be.visible')
    })
})