const pkg = require("./package.json");

module.exports = {
    apiPath: "stubs/api",
    webpackConfig: {
        output: {
            publicPath: `/static/${pkg.name.replace("uds-", "")}/${process.env.VERSION || pkg.version}/`,
        }
    },
    config: {
        'orgs.api.base.url': '/api'
    },
    apps: {
        orgs: { name: 'orgs', version: process.env.VERSION || pkg.version }
    },
    navigations: {
        'main': '/main',
        'orgs': '/orgs',
        'news': '/news',
        'sections': '/sections',
    }
};
