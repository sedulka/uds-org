let execSync = require('child_process').execSync;


const devDependencies = require("./package.json").devDependencies;
checkUpdates(devDependencies, "devDependencies");

const dependencies = require("./package.json").dependencies;
checkUpdates(dependencies, "dependencies");
/*
    Словари модулей добавлять так же как сверху
 */


/*
    Основной метод для проверки обновлений
 */
function checkUpdates(modulesDict, modulesDictName) {
    console.log("Checking updates for \"" + modulesDictName + "\":");

    let isEverythingUpdated = true;
    for (let moduleName in modulesDict) {
        let moduleVersion = modulesDict[moduleName];
        try {
            const output = execSync('npm view ' + moduleName + " version", {encoding: 'utf-8'});

            let moduleAvailableVersion = output.trim();

            switch (moduleVersion.charAt(0)) {
                case "^":
                    if (getMajorNumber(moduleVersion.substr(1)) !== getMajorNumber(moduleAvailableVersion)) {
                        isEverythingUpdated = false;
                        logAvailableUpdate(moduleName, moduleVersion, moduleAvailableVersion);
                    }
                    break;
                case "~":
                    if (getMajorAndMinorNumber(moduleVersion.substr(1))
                        !== getMajorAndMinorNumber(moduleAvailableVersion)) {
                        isEverythingUpdated = false;
                        logAvailableUpdate(moduleName, moduleVersion, moduleAvailableVersion);
                    }
                    break;
                default:
                    if (moduleVersion !== moduleAvailableVersion) {
                        isEverythingUpdated = false;
                        logAvailableUpdate(moduleName, moduleVersion, moduleAvailableVersion);
                    }
                    break;
            }
        } catch (e) {
            console.error('[ERROR] Update-check command: ' + e);
            // не уверен насчет формата вывода ошибки, но так вроде тоже сойдет
        }
    }
    if (isEverythingUpdated) {
        console.log("Everything is up to date.\n")
    } else {
        console.log("Modules above are outdated. Others are up to date.\n")
    }
}

/*
    Вывод доступного обновления
 */
function logAvailableUpdate(moduleName, olderVersion, newerVersion) {
    if (olderVersion !== newerVersion) {
        console.log(
            "   \"" + moduleName + "\"\n" +
            "      required:  " + olderVersion + "\n" +
            "      available: " + newerVersion + "\n"
        );
    }
}


/*
    Major, minor, patch numbers semantics:
    https://bytearcher.com/goodies/semantic-versioning-cheatsheet/
 */
function getMajorNumber(version) {
    return version.slice(0, version.indexOf("."));
}

function getMajorAndMinorNumber(version) {
    return version.slice(0, version.lastIndexOf("."));
}
