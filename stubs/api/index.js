const router = require('express').Router();
const orgData = require('./org.data.json')
const orgsData = require('./orgs.data.json')
const markerData = require('./markers.data.json')

const waitMiddleWare = (req, res, next) => {
    // setTimeout(next, 1000)
}

router.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "http://89.223.91.151:8080");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

router.get('/getOrgsData', (req, res) => {
    res.send(orgsData)
})

router.get('/getOrgData', (req, res) => {
    res.send(orgData)
})

router.get('/getMarkerData', (req, res) => {
    res.send(markerData)
})

module.exports = router;