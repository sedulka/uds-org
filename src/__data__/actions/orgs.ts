import axios, { AxiosRequestConfig, AxiosResponse } from 'axios'
import { getConfig } from '@ijl/cli'

import {
    ORGS_DATA_FETCH,
    ORGS_DATA_FETCH_FAIL,
    ORGS_DATA_FETCH_SUCCESS
} from '../constants/action-types'

import { OrgsDataResponse } from '../model/interfaces'

const api = getConfig()['orgs.api.base.url']


const getFetchAction = () => ({
    type: ORGS_DATA_FETCH,
})

const getSuccessAction = (data) => ({
    type: ORGS_DATA_FETCH_SUCCESS,
    data
})

const getErrorAction = () => ({
    type: ORGS_DATA_FETCH_FAIL,
})


export default () => async (dispatch: any) => {
    dispatch(getFetchAction())

    const requestProps: AxiosRequestConfig = {
        method: 'get',
        headers: {
            'Content-Type': 'application/json',
        }
    };

    try {
        const answer: AxiosResponse<OrgsDataResponse> = await axios(`${api}/getOrgsData`, requestProps);

        if (answer.data?.status?.code === 0) {
            dispatch(getSuccessAction(answer.data));
        } else {
            dispatch(getErrorAction());
        }

    } catch (error) {
        dispatch(getErrorAction());
    }
}