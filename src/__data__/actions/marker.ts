import axios, { AxiosRequestConfig, AxiosResponse } from 'axios'

import {
    MARKER_DATA_FETCH,
    MARKER_DATA_FETCH_FAIL,
    MARKER_DATA_FETCH_SUCCESS
} from '../constants/action-types'

import { MarkerDataResponse } from '../model/interfaces'


const getFetchAction = () => ({
    type: MARKER_DATA_FETCH,
})

const getSuccessAction = (data) => ({
    type: MARKER_DATA_FETCH_SUCCESS,
    data
})

const getErrorAction = () => ({
    type: MARKER_DATA_FETCH_FAIL,
})


export default () => async (dispatch: any) => {
    dispatch(getFetchAction())

    const requestProps: AxiosRequestConfig = {
        method: 'get',
        headers: {
            'Content-Type': 'application/json',
        }
    };

    try {
        const answer: AxiosResponse<MarkerDataResponse> = await axios('/api/getMarkerData', requestProps);

        if (answer.data?.status?.code === 0) {
            dispatch(getSuccessAction(answer.data));
        } else {
            dispatch(getErrorAction());
        }

    } catch (error) {
        dispatch(getErrorAction());
    }
}