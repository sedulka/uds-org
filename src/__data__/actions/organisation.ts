import axios, { AxiosRequestConfig, AxiosResponse } from 'axios'
import { getConfig } from '@ijl/cli'

import {
    ORGANISATION_DATA_FETCH,
    ORGANISATION_DATA_FETCH_SUCCESS,
    ORGANISATION_DATA_FETCH_FAIL
} from '../constants/action-types'

import { OrgDataResponse } from '../model/interfaces'

const api = getConfig()['orgs.api.base.url']

const getFetchAction = () => ({
    type: ORGANISATION_DATA_FETCH,
})

const getSuccessAction = (data) => ({
    type: ORGANISATION_DATA_FETCH_SUCCESS,
    data
})

const getErrorAction = () => ({
    type: ORGANISATION_DATA_FETCH_FAIL,
})


export default (id: number) => async (dispatch: any) => {
    dispatch(getFetchAction())

    const requestProps: AxiosRequestConfig = {
        method: 'get',
        params: {
            id: id
        },
        headers: {
            'Content-Type': 'application/json',
        }
    };

    try {
        const answer: AxiosResponse<OrgDataResponse> = await axios(`${api}/getOrgData`, requestProps);

        if (answer.data?.status?.code === 0) {
            dispatch(getSuccessAction(answer.data.org));
        } else {
            dispatch(getErrorAction());
        }

    } catch (error) {
        dispatch(getErrorAction());
    }
}