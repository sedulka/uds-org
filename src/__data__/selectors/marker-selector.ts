import { createSelector } from "reselect";

const getStore = state => state

export const getMarkerData = createSelector(getStore, (state) => state?.marker?.data)

export const getMarkerDataLoading = createSelector(getStore, (state) => state?.marker?.loading)

export const getAll = createSelector(
    getMarkerData,
    (data) => data ? data.orgs : null
)

