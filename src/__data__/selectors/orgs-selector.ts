import { createSelector } from "reselect";

const getStore = state => state

const getOrgsData = createSelector(getStore, (state) => state?.orgs?.data?.orgs)

export const getOrgsDataLoading = createSelector(getStore, (state) => state?.orgs?.loading)

export const getOrgsDataError = createSelector(getStore, (state) => state?.orgs?.error)

export const getDistricts = createSelector(
    getOrgsData,
    (data) => data ? Object.keys(data) : null
)

export const getAreas = createSelector(
    [getOrgsData, getDistricts],

    (data, districts) => {
        let areas = new Object()
        districts?.forEach(dist => {
            areas[dist] = Object.keys(data[dist])
        })
        return areas
    }
)

export const getOrgs = createSelector(
    [getOrgsData, getDistricts, getAreas],

    (data, districts, areas) => {
        let orgs = new Object()
        districts?.forEach(dist => {
            Object.keys(data[dist])?.forEach(area => {
                orgs[area] = data[dist][area]
            })
        })
        return orgs
    }
)

export const getSectionsTypes = createSelector(
    getStore,
    (state) => state.orgs?.data?.types
)

