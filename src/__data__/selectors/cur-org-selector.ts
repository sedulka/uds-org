import { createSelector } from "reselect";

const getStore = state => state
const getCurOrgData = createSelector(getStore, (state) => state?.curOrg?.data)

export const getOrgDataLoading = createSelector(getStore, (state) => state?.curOrg?.loading)
export const getOrgDataError = createSelector(getStore, (state) => state?.curOrg?.error)

export const getOrgName = createSelector(
    getCurOrgData,
    (data) => data?.name || ''
)

export const getOrgDescription = createSelector(
    getCurOrgData,
    (data) => data?.description || ''
)

export const getOrgAddress = createSelector(
    getCurOrgData,
    (data) => data?.address || ''
)

export const getOrgAdministration = createSelector(
    getCurOrgData,
    (data) => data?.administration || []
)

export const getOrgSections = createSelector(
    getCurOrgData,
    (data) => divideSectionsByAges(data?.sections, data?.divideSectionsByAges) || {}
)

export const getOrgLinks = createSelector(
    getCurOrgData,
    (data) => data?.links || {}
)

export const getOrgAbout = createSelector(
    getCurOrgData,
    (data) => data?.about || {}
)

export const getOrgAwards = createSelector(
    getCurOrgData,
    (data) => data?.awards || []
)

export const getOrgTeachingStaff = createSelector(
    getCurOrgData,
    (data) => data?.teachingStaff || {}
)

export type Sections = {
    range: number[],
    sections: {
        name: string,
        grade: number
    }[]
}[]

// todo: use gender type declaration as boolean instead of string
export type Administrators = {
    name: string,
    post: string,
    gender: 'female' | 'male'
}[]

export type CreativeStudioLeader = {
    name: string,
    post: string,
    gender: 'female' | 'male',
    description: string
}

export type SportsInstructor = {
    name: string,
    post: string,
    gender: 'female' | 'male',
    description: string
}

export type TeachingStaff = {
    creativeStudioLeaders: CreativeStudioLeader[],
    sportsInstructors: SportsInstructor[]
}


const divideSectionsByAges = (sections, ranges: Array<Array<number>>): Sections => {
    if (!sections || !ranges) {
        return
    }

    const sortedRanges = ranges.sort((a, b) => a[0] < b[0] ? -1 : 1)
    let sectionsByAges = sortedRanges.map((el) => {
        return {
            range: el,
            sections: []
        }
    })

    Object.keys(sections).forEach(key => {
        const section = sections[key]
        // Get lower and upper bounds of section
        const lower = Math.min(section.ageRange[0], section.ageRange[1])
        const upper = Math.max(section.ageRange[0], section.ageRange[1])

        for (let i = 0; i < sortedRanges.length; i++) {
            // If the lower or upper between range add to the sections
            const expr1 = sortedRanges[i][0] <= lower && lower <= sortedRanges[i][1]
            const expr2 = sortedRanges[i][0] <= upper && upper <= sortedRanges[i][1]

            if (expr1 || expr2) {
                delete section.ageRange
                sectionsByAges[i].sections.push(section)
            }
        }
    });

    return sectionsByAges
}
