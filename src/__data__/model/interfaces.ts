export interface OrgDataResponse {
    status: Status;
    org: Organisation;
}

export interface Organisation {
    name: string;
    description: string;
    address: string;
    sections: Section[];
    about: {
        videoURL: string;
        description: string;
    }
    awards: Award[];
    divideSectionsByAges: number[][];
    admins: Admin[];
    teachingStaff: TeachingStaff
}

export interface Award {
    place: string;
    date: string;
    description: string;
}

export interface Section {
    name: string;
    ageRange: number[];
    grade: number;
}

export interface Admin {
    name: string;
    post: string;
    gender: 'female' | 'male'
}

export interface CreativeStudioLeader {
    name: string,
    post: string,
    gender: 'female' | 'male',
    description: string
}

export interface SportsInstructor {
    name: string,
    post: string,
    gender: 'female' | 'male',
    description: string
}

export interface TeachingStaff {
    creativeStudioLeaders: CreativeStudioLeader[],
    sportsInstructors: SportsInstructor[]
}

export interface OrgsDataResponse {
    status: Status;
    orgs: District[];
}

export interface District {
    [district: string]: Area[];
}

export interface Area {
    [area: string]: string[];
}

export interface Status {
    code: number;
}

export interface MarkerDataResponse {
    status: Status;
    orgs: Part[];
}

export interface Part {
    [part: string]: Place[];
}

export interface Place {
    [place: string]: Info[];
}

export interface Info {
    address: string;
    sections: string[];
    coordinates: number[];
}