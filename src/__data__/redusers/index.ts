import { combineReducers } from 'redux'
import { orgsReducer } from './orgsReducer'
import { curOrgReducer } from './curOrgReducer'
import { markerReducer } from './markerReducer'

const reducers = combineReducers({
    orgs: orgsReducer,
    curOrg: curOrgReducer,
    marker: markerReducer
})

export default reducers