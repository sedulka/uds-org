import {
    ORGANISATION_DATA_FETCH,
    ORGANISATION_DATA_FETCH_FAIL,
    ORGANISATION_DATA_FETCH_SUCCESS
} from '../constants/action-types'

const initialState = {
    data: null,
    loading: false,
    error: null
}

const fetchHandler = (state, action) => ({
    ...state,
    loading: true
})

const fetchSuccessHandler = (state, action) => ({
    ...state,
    loading: false,
    data: action.data,
})

const fetchFailHandler = (state, action) => ({
    ...state,
    loading: false,
    error: action.error || true,
    data: null
})

const handlers = {
    [ORGANISATION_DATA_FETCH]: fetchHandler,
    [ORGANISATION_DATA_FETCH_SUCCESS]: fetchSuccessHandler,
    [ORGANISATION_DATA_FETCH_FAIL]: fetchFailHandler,
}

export const curOrgReducer = (state = initialState, action) =>
    Object.prototype.hasOwnProperty.call(handlers, action.type) ? handlers[action.type](state, action) : state;