import {
    MARKER_DATA_FETCH,
    MARKER_DATA_FETCH_SUCCESS,
    MARKER_DATA_FETCH_FAIL
} from '../constants/action-types'

const initialState = {
    data: null,
    loading: false,
    error: null
}

const fetchHandler = (state, action) => ({
    ...state,
    loading: true
})

const fetchSuccessHandler = (state, action) => ({
    ...state,
    loading: false,
    data: action.data,
})

const fetchFailHandler = (state, action) => ({
    ...state,
    loading: false,
    error: action.error,
    data: null
})

const handlers = {
    [MARKER_DATA_FETCH]: fetchHandler,
    [MARKER_DATA_FETCH_SUCCESS]: fetchSuccessHandler,
    [MARKER_DATA_FETCH_FAIL]: fetchFailHandler,
}

export const markerReducer = (state = initialState, action) =>
    Object.prototype.hasOwnProperty.call(handlers, action.type) ? handlers[action.type](state, action) : state;