import LogoIcon from './icon.svg'
import StarIcon from './star.svg'
import PDFIcon from './pdf.svg'
import PlusIcon from './plus.svg'
import MinusIcon from './minus.svg'
import CloseIcon from './ico-close.svg'
import FilterIcon from './filter.svg'
import WreathIcon from './wreath.svg'

export {
    LogoIcon,
    StarIcon,
    PDFIcon,
    PlusIcon,
    MinusIcon,
    CloseIcon,
    FilterIcon,
    WreathIcon
}