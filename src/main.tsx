import React, { Component, useDebugValue } from 'react';
import ReactDom, { render } from 'react-dom';
import App from './App';

export default () => <App />

export const mount = (Сomponent) => {
    ReactDom.render(
        <Сomponent />,
        document.getElementById('app')
    );
}

export const unmount = () => {
    ReactDom.unmountComponentAtNode(document.getElementById('app'))
}

// @ts-ignore
if (module.hot) {
    // @ts-ignore
    module.hot.accept('./App.tsx', () => {
        ReactDom.render(
            <App />,
            document.getElementById('app')
        );
    });
}

