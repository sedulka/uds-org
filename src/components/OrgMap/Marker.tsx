import React from 'react';
import {
    MarkerWrapper,
    MarkerBottom,
    SectionText,
    OrgText,
    AddressText,
    EnrollLong
} from './styled'
import {CloseIcon} from '@main/assets'

const Marker = (props: any) => {
    return (
        <div style={{marginLeft: "-124px", marginTop: "-220px"}}>
            <MarkerWrapper>
                <div style={{padding: "8px 16px 16px 16px"}}>
                    <img src={CloseIcon} onClick={props.closeMarker} style={{marginLeft: "210px", cursor: "pointer"}}/>
                    <SectionText>
                    {props.section[0]}
                    </SectionText>
                    <OrgText>{props.org}</OrgText>
                    <AddressText>{props.address}</AddressText>
                    <EnrollLong>Записаться</EnrollLong>
                </div>
            </MarkerWrapper>
            <MarkerBottom/>
        </div>
        
    );
}

export default Marker;