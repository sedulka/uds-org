import React, { useState } from 'react';
import GoogleMapReact from 'google-map-react';
import Marker from './Marker'
import Dot from './Dot';
import { getAll } from '@main/__data__/selectors/marker-selector';
import { connect } from 'react-redux';

import {
    GoogleMapContainer
} from './styled'

const SimpleMap = ({ markers, area, chosen, types, marker, setMarker }) => {
    const [center, setCenter] = useState({ lat: 55.751244, lng: 37.618423 });
    const [zoom, setZoom] = useState(14);

    const returnMarker = (el) => {
        setMarker(<Marker
            lat={markers[area][el].coordinates[0]}
            lng={markers[area][el].coordinates[1]}
            org={el}
            section={markers[area][el].sections}
            address={markers[area][el].address}
            closeMarker = {closeMarker}
        />)
    }

    const closeMarker = () => {
        setMarker(null)
    }

    return (
        <GoogleMapContainer>
            <GoogleMapReact
                bootstrapURLKeys={{ key: 'AIzaSyAfjbHHwyxcRXVQ6hSkb5dz9zrjma0Z_FM' }}
                defaultCenter={center}
                defaultZoom={zoom}
            >
                {markers !== null && markers[area] !== undefined && Object.keys(markers[area]).map((el, i) => (
                    (chosen.length === 1 || chosen.indexOf(true) === -1 || chosen.indexOf(false) === -1) ?
                    
                            <Dot
                                key={i}
                                returnMarker={returnMarker}
                                el={el}
                                lat={markers[area][el].coordinates[0]}
                                lng={markers[area][el].coordinates[1]}
                            />
                        
                    :

                        types.map((type, ind) => markers[area][el].sections
                        .filter(element => element === type && chosen[ind] === true)
                        .map(() => (
                                        <Dot
                                            key={i}
                                            returnMarker={returnMarker}
                                            el={el}
                                            lat={markers[area][el].coordinates[0]}
                                            lng={markers[area][el].coordinates[1]}
                                        />
                                    )
                            
                            ))
                
                ))}
                {marker !== null && marker}
            </GoogleMapReact>
        </GoogleMapContainer>
    );
}

const mapStateToProps = (state, props) => {
    return {
        markers: getAll(state),
        area: props.area
    }
}

export default connect(mapStateToProps)(SimpleMap);