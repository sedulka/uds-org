import React, { MouseEvent, FormEvent, useState } from 'react';
import {
    DotMarker
} from './styled'

const Dot = (props: any) => {
    return (
        <>
        <DotMarker
        onClick = {() => {props.returnMarker(props.el, props.key)}}
        />
        </>
    );
}

export default Dot;