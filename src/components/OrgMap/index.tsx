import React, { MouseEvent, FormEvent, useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect'
import { bindActionCreators } from 'redux';
import { Button } from '@ijl/uds-ui'

import {
    CheckboxesWrapper,
    Label,
    SectionsWrapper,
    Select,
    ButtonDisabled,
    ButtonEnabled,
    FormContainer,
    MapContainer,
    MapAndFormContainer,
    FilterButton,
    ButtonContainer1,
    ButtonContainer2,
    SectionsContainer,
    FilterDescriptionWrapper,
    CrossWrapper
} from './styled'
import MyMapComponent from './GoogleMap';
import { getAreas, getSectionsTypes } from '@main/__data__/selectors/orgs-selector';
import { getAll } from '@main/__data__/selectors/marker-selector';
import fetchMarkerData from '@main/__data__/actions/marker';
import { FilterIcon, CloseIcon } from '@main/assets';

interface OrgMapProps {
    areas: string[];
    types: string[];
    marker: any;
    fetchMarkerData: Function;
}

const OrgMap: React.FC<OrgMapProps> = ({ areas, types, marker, fetchMarkerData }) => {
    useEffect(() => { fetchMarkerData() }, [])
    const [area, setArea] = useState("ЦАО");
    const [forceFormVisibility, setForceFormVisibility] = React.useState(false);
    const [chosen, setChosen] = useState([false])
    const [center, setCenter] = useState({ lat: 11.0168, lng: 76.9558 })
    const [zoom, setZoom] = useState(11)

    const [mark, setMark] = useState(null)
    let clubs = []

    const handleClick = (event: MouseEvent<HTMLButtonElement>, val: boolean) => {
        // Handle select and deselect checkboxes 
        event.preventDefault();
        setChosen(new Array(types.length).fill(val))
    }

    const handleChange = (event: FormEvent<HTMLSelectElement>) => {
        // Handle change of area selection
        setMark(null);
        setArea(event.currentTarget.value);
    }

    const handleCheckBox = (i: number) => {
        // Handle checkbox of types of sections
        let tmp = [...chosen]
        tmp[i] = !tmp[i]
        setChosen(tmp)
    }



    // Options of areas initialization
    const options = Object.keys(areas).map(val => <option value={val} key={val}>{val}</option>);

    // Checkboxes of section types initialization
    const checkboxes = types.map((val, i) => (
        <div style={{ marginTop: "23px" }} onClick={() => handleCheckBox(i)}>
            <input style={{ marginRight: "18px" }}
                name="sections"
                type="checkbox"
                checked={chosen[i]}
                key={i}
            />
            <label style={{ fontFamily: "Montserrat", fontStyle: "normal", fontWeight: 'normal', fontSize: '16px' }} htmlFor="sections">{val}</label>
        </div>
    ))

    // Select all class
    const SelectAll = !Object.values(chosen).every(item => item === true) ? ButtonEnabled : ButtonDisabled
    const DeselectAll = !Object.values(chosen).every(item => item === false) ? ButtonEnabled : ButtonDisabled

    return (
        <>
            <FilterButton onClick={() => setForceFormVisibility(!forceFormVisibility)}>
                <div>Фильтр</div>
                <img src={FilterIcon} />
            </FilterButton>
            <MapAndFormContainer>
                <FormContainer style={forceFormVisibility ? { display: 'block' } : null}>
                    <form>
                        <CrossWrapper onClick={() => setForceFormVisibility(!forceFormVisibility)}>
                            <img src={CloseIcon}></img>
                        </CrossWrapper>
                        {/* Select the area */}
                        <Label htmlFor="area">
                            Округ или район
                        </Label>
                        <Select
                            value={area}
                            onChange={handleChange}
                            name="area"
                        >
                            {options}
                        </Select>

                        {/* Checkbox for types of sections */}
                        <Label htmlFor="section">Секции</Label>
                        <SectionsWrapper>
                            {/* Select and deselect buttons  */}
                            <div style={{ marginBottom: "10px" }}>
                                <SelectAll
                                    onClick={(event) => handleClick(event, true)}
                                >
                                    Выбрать все
                            </SelectAll>
                                <DeselectAll
                                    onClick={(event) => handleClick(event, false)}
                                >
                                    Cнять выделение
                            </DeselectAll>
                            </div>

                            {/* Checkboxes */}
                            <CheckboxesWrapper>
                                {checkboxes}
                            </CheckboxesWrapper>
                            <ButtonContainer1>
                                <Button>Записаться</Button>
                            </ButtonContainer1>
                        </SectionsWrapper>
                    </form>
                </FormContainer>
                {/* Button for sign up to the section */}
                <SectionsContainer>
                    <FilterDescriptionWrapper>
                        Район: {area} <br/><br/>
                        Клубы:  <br /><br />
                    </FilterDescriptionWrapper>
                    <ButtonContainer2>
                        <Button>Записаться</Button>
                    </ButtonContainer2>
                </SectionsContainer>
                <MapContainer>
                    <MyMapComponent area={area} chosen={chosen} marker={mark} setMarker={setMark} types={types} />
                </MapContainer>
            </MapAndFormContainer>
        </>
    )
}

const mapStateToProps = () => createStructuredSelector({
    types: getSectionsTypes,
    areas: getAreas,
    marker: getAll
})

const mapDispatchToProps = (dispatch: any) => bindActionCreators({
    fetchMarkerData
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(OrgMap)