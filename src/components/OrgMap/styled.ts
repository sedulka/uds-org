import styled from 'styled-components'

export const CheckboxesWrapper = styled.div`
    border-top: 1px solid #E4E4E4;
    overflow: auto; 
    max-height: 300px;
    border-bottom: 1px solid #E4E4E4;
`

export const SectionsWrapper = styled.div`
    background: #FFFFFF;
    border: 1px solid #E4E4E4;
    border-radius: 3px; 
    width: 100%; 
    height: 430px;

    @media (max-width: 810px) {
        height: 400px;
        border: none;
        border-radius: 3px;
    }
`

export const MarkerWrapper = styled.div`
    width: 244px;
    height: 185px;
    display: block;
    background: #FFFFFF;
    box-shadow: 0px 8px 24px rgba(0, 0, 0, 0.159873);
    border-radius: 3px;
    z-index:9999;
    position: absolute;
`

export const FormContainer = styled.div`
    padding: 0px;
    width: 100%;
    height: auto;
    display: block;
    flex-basis: 40%;

    @media (max-width: 810px) {
        display: none;
        position: fixed;
        overflow: auto;
        left: 0px;
        top: 0px;
        z-index: 99999;
        background: white;
        padding: 20px 10px;
        width: calc(100vw - 20px);
        height: 100vh;
    }
`

export const SectionsContainer = styled.div`

    display: flex; 
    justify-content: center; 
    flex-direction: column; 
    align-items: center;

    @media (min-width: 810px) {
        display: none;
    }
`

export const CrossWrapper = styled.div`

    display: block; 
    text-align: end;
    cursor: pointer;

    @media (min-width: 810px) {
        display: none;
    }
`


export const ButtonContainer1 = styled.div`

    display: none;

    @media (min-width: 810px) {
        display: flex;
        justify-content: center;
        align-items: center;
        padding: 15px;
    }
`

export const FilterDescriptionWrapper = styled.div`

    font-family: Montserrat;
    font-style: normal;
    font-weight: normal;
    font-size: 13px;
    line-height: 12px;
    display: block;
    width: 100%;
    
    @media (min-width: 810px) {
        display: none;
    }
`

export const ButtonContainer2 = styled.div`

    padding: 5px 20px;
    width: 100%;

    @media (min-width: 810px) {
        display: none;
    }
`

export const MapContainer = styled.div`

    width: 100%;

    @media (min-width: 810px) {
        flex-basis: 60%;
        margin-left: 20px;
    }
`

export const MapAndFormContainer = styled.div`
    display: block;
    margin-bottom: 140px;

    @media (min-width: 810px) {
        display: flex;
        margin-bottom: 140px;
    }
`

export const GoogleMapContainer = styled.div`
    height: 320px; 
    width: 100%;

    @media (min-width: 810px) {
        height: 535px;
    }
`

export const MarkerBottom = styled.div`
    height: 30px;
    width: 30px;
    margin-left: 110px;
    background: #FFFFFF;
    border-radius: 2px;
    transform: rotate(45deg);
    z-index:999;
    position: relative;
    bottom: -158px;
`
export const DotMarker = styled.div`
    height: 25px;
    width: 25px;
    background-color: #003EFF;;
    border-radius: 50%;
    border: 4px solid #FFFFFF;
    cursor: pointer;
    margin-top: -19px;
    margin-left: -15px;
`
export const SectionText = styled.p`
    font-family: Montserrat;
    font-style: normal;
    font-weight: bold;
    font-size: 16px;
    line-height: 28px;
    height: 28px;
    left: 6.56%;
    right: 6.56%;
    top: calc(50% - 28px/2 - 67px);
    color: #1E1D20;
    margin-top: 0px;
`

export const OrgText = styled.p`
    height: 12px;
    left: 6.56%;
    right: 6.56%;
    margin-top: 0px;
    font-family: Montserrat;
    font-style: normal;
    font-weight: bold;
    font-size: 10px;
    line-height: 12px;
    text-transform: uppercase;
    color: #003EFF;
`
export const AddressText = styled.p`
    height: 28px;
    left: 6.56%;
    right: 6.56%;
    font-family: Montserrat;
    font-style: normal;
    font-weight: normal;
    font-size: 12px;
    line-height: 14px;
    color: #1E1D20;
`
export const EnrollLong = styled.button`
    background: #003EFF;
    border-radius: 28px;
    font-family: Montserrat;
    font-style: normal;
    font-weight: bold;
    font-size: 14px;
    line-height: 17px;
    text-align: center;
    color: #FFFFFF;
    width: 100%;
    height: 28px;
`


export const Label = styled.label`
    height: 12px;
    right: 76.96%;
    top: calc(50% - 12px/2 - 24px);

    font-family: Montserrat;
    font-style: normal;
    font-weight: bold;
    font-size: 10px;
    line-height: 12px;
    text-transform: uppercase;

    color: #1E1D20;

    mix-blend-mode: normal;
    opacity: 0.5;
`

export const Select = styled.select`
    width: 100%;
    padding: 12px 5px 11px 16px;
    font-size: 16px;
    font-family: Montserrat;
    font-style: normal;
    font-weight: normal;
    border: 1px solid #E4E4E4;
    border-radius: 3px;
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    margin-bottom: 24px;
    left: 0%;
    right: 0%;
    top: 28.33%;
    bottom: 0%;
    background: url(https://img.icons8.com/color/96/000000/expand-arrow.png) 96% center / 5% no-repeat;
`

export const ButtonEnabled = styled.button`
    background: none;
    box-shadow: 0px 0px 0px transparent;
    border: 0px solid transparent;
    text-shadow: 0px 0px 0px transparent;

    border: none;
    height: 34px;
    left: 0%;
    right: 0%;
    top: calc(50% - 34px/2);

    font-family: Montserrat;
    font-style: normal;
    font-weight: bold;
    font-size: 12px;
    line-height: 34px;
    color: #003EFF;

    border-bottom: 1px dashed rgba(0, 62, 255, 0.30);
`

export const ButtonDisabled = styled.button`
    background: none;
    box-shadow: 0px 0px 0px transparent;
    border: 0px solid transparent;
    text-shadow: 0px 0px 0px transparent;

    border: none;
    font-family: Montserrat;
    font-style: normal;
    font-weight: bold;
    font-size: 12px;
    line-height: 34px;
    color: #003EFF;
    height: 34px;
    left: 5.22%;
    right: 76.74%;
    top: calc(50% - 34px/2 - 255.5px);

    mix-blend-mode: normal;
    opacity: 0.3;
    border-bottom: none;
`

export const FilterButton = styled.div`
    font-family: Montserrat;
    font-size: 12px;
    line-height: 12px;
    width: 80px;
    display: flex;
    justify-content: space-between;
    align-items: center;
    color: #003EFF;
    float: right;
    margin-top: -50px;
    @media (min-width: 810px) {
        display: none;
    }
`