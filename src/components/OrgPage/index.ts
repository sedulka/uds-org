import Sections from './Sections'
import Administration from "./Administration";
import TeachingStaff from "./TeachingStaff";

export {
    Sections,
    Administration,
    TeachingStaff
}