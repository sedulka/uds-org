import React from 'react';
import {Typography} from '@ijl/uds-ui';
import {WreathIcon} from '@main/assets'

import {
    CenterText,
    AwardIconContainer,
    AwardContainer,
    WreathImage,
    PlaceNumber,
    PlaceLabel
} from './styled'
import {AwardType} from "@main/components/OrgPage/Awards/index";

const Award: React.FC<AwardType> = ({place, date, description}) => {
    return (
        <AwardContainer>
            <PlaceNumber>
                <Typography
                    component="p"
                    fontSize="68px"
                    fontWeight={700}
                >
                    {place}
                </Typography>
            </PlaceNumber>
            <PlaceLabel>
                <Typography
                    component="p"
                    fontSize="22px"
                    fontWeight={600}
                >
                    {"место"}
                </Typography>
            </PlaceLabel>
            <AwardIconContainer>
                <WreathImage
                    src={WreathIcon}
                />
            </AwardIconContainer>
            <CenterText>
                <Typography
                    component="p"
                    fontSize="22px"
                    fontWeight={600}
                    margin="0 0 8px 0"
                >
                    {date}
                </Typography>
                <Typography
                    component="p"
                    fontSize="16px"
                    fontWeight={400}
                >
                    {description}
                </Typography>
            </CenterText>
        </AwardContainer>
    )
}

export default Award