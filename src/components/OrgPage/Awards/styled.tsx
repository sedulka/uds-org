import styled from 'styled-components'

const Wrapper = styled.div`
    display: -webkit-flex;
    display: flex;
    -webkit-flex-flow: row wrap;
    flex-flow: row wrap;
    align-content: center;
`;

export const Header = styled(Wrapper)`
    margin-top: 64px;
    position: relative;
    justify-content: center;
    align-items: center;

    margin-bottom: 40px;

    @media (min-width: 810px) {
        margin-top: 100px;
    }

    h1 {
        flex: 1;
        font-size: 22px;
        text-align: center;

        @media (min-width: 810px) {
            font-size: 50px;
        }
    }

    h2 {
        margin-left: auto;

        font-size: 10px;
        font-weight: normal;
        line-height: 12px;


        @media (min-width: 810px) {
            position: absolute; 
            top: 0;
            right: 13px;

            font-size: 22px;
            font-weight: 600;
            line-height: 27px;

            padding: 33px;
        }
    }

    h2:hover {
        cursor: pointer;
        text-decoration: underline;
    }
`;

export const AwardsBlock = styled(Wrapper)`
    justify-content: center;
`;

export const AwardContainer = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: center;
    max-width: 300px;
    margin-bottom: 20px;
`;

export const AwardIconContainer = styled.div`
    height: 168px;
    width: 168px;
    margin-bottom: 13px;
    flex-shrink: 0;
`;

export const CenterText = styled.span`
    text-align: center;
`;

export const WreathImage = styled.img`
    height: 100%;
    width: 100%;
    border-radius: 50%;
`;

export const PlaceNumber = styled.div`
    text-align: center;
    height: 0px;
    position: relative;
    top: 7%
`;

export const PlaceLabel = styled.div`
    text-align: center;
    height: 0px;
    position: relative;
    top: 30%
`;