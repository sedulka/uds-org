import React from 'react'
import {connect} from 'react-redux'
import {createStructuredSelector} from 'reselect'
import {Typography, PersonVertical} from '@ijl/uds-ui'
import {getOrgAwards} from '@main/__data__/selectors/cur-org-selector'
import {Header, AwardsBlock} from './styled'
import Award from "@main/components/OrgPage/Awards/Award";

export type AwardType = {
    place: string;
    date: string;
    description: string;
}

type AwardsProps = {
    awards: AwardType[];
}

const Awards: React.FC<AwardsProps> = ({awards}) => {
    const content = awards?.map((award) => (
        <Award
            place={award.place}
            date={award.date}
            description={award.description}
        />
    ))

    return (
        <>
            <Header>
                <Typography
                    component='h1'
                    fontWeight={600}
                >
                    Награды
                </Typography>
            </Header>

            <AwardsBlock key={"awards"}>
                {content}
            </AwardsBlock>
        </>
    )
}

const mapStateToProps = () => createStructuredSelector({
    awards: getOrgAwards
})

export default connect(mapStateToProps)(Awards)