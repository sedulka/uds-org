import React from 'react';
import {Typography, PersonHorizontal} from '@ijl/uds-ui';
import {SportsInstructor} from "@main/__data__/selectors/cur-org-selector";
import {TeachingStaffPersonBlock, Line} from "@main/components/OrgPage/TeachingStaff/styled";

type SportsInstructorProps = {
    instructors: SportsInstructor[]
}

const SportsInstructors: React.FC<SportsInstructorProps> = ({instructors}) => {
    const content = instructors?.map((instructor, index) => (
        <>
            <TeachingStaffPersonBlock key={instructor.name}>
                <PersonHorizontal
                    gender={instructor.gender}
                    name={instructor.name}
                    jobTitle={instructor.post}
                    jobDescription={instructor.description}
                />
            </TeachingStaffPersonBlock>
            {(index === instructors.length - 1) ? <></> : <Line/>}
        </>
    ))

    return (
        <>
            {content}
        </>
    )
}

export default SportsInstructors