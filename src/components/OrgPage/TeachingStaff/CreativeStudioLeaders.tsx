import React from 'react';
import {PersonHorizontal} from '@ijl/uds-ui';
import {CreativeStudioLeader} from "@main/__data__/selectors/cur-org-selector";
import {Line, TeachingStaffPersonBlock} from "@main/components/OrgPage/TeachingStaff/styled";

type CreativeStudioLeaderProps = {
    leaders: CreativeStudioLeader[]
}

const CreativeStudioLeaders: React.FC<CreativeStudioLeaderProps> = ({leaders}) => {
    const content = leaders?.map((leader, index) => (
        <>
            <TeachingStaffPersonBlock key={leader.name}>
                <PersonHorizontal
                    gender={leader.gender}
                    name={leader.name}
                    jobTitle={leader.post}
                    jobDescription={leader.description}
                />
            </TeachingStaffPersonBlock>
            {(index === leaders.length - 1) ? <></> : <Line/>}
        </>
    ))

    return (
        <>
            {content}
        </>
    )
}

export default CreativeStudioLeaders