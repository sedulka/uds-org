import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux'
import { Typography } from '@ijl/uds-ui'
import { createStructuredSelector } from 'reselect';

import SwitchButton from '@main/components/SwitchButton'
import { switchButtonStyles, TeachingStaffBlock } from './styled'
import {
    CreativeStudioLeader,
    getOrgTeachingStaff,
    SportsInstructor,
    TeachingStaff as TeachingStaffModel,
} from '@main/__data__/selectors/cur-org-selector'
import CreativeStudioLeaders from "@main/components/OrgPage/TeachingStaff/CreativeStudioLeaders";
import SportsInstructors from "@main/components/OrgPage/TeachingStaff/SportsInstructors";
import { Header } from "@main/components/OrgPage/Administration/styled";


type TeachingStaffProps = {
    teachingStaff: TeachingStaffModel
}

const TeachingStaff: React.FC<TeachingStaffProps> = ({ teachingStaff }) => {

    const [isLeftButtonSelected, setIsLeftButtonSelected] = useState(true)

    let content: JSX.Element;
    if (isLeftButtonSelected) {
        content = <SportsInstructors
            instructors={teachingStaff.sportsInstructors}
        />;
    } else {
        content = <CreativeStudioLeaders
            leaders={teachingStaff.creativeStudioLeaders}
        />;
    }

    const handleSwitch = () => setIsLeftButtonSelected(prev => !prev)

    //  Take the content of the page based on isList variable


    return (
        <>
            <Header>
                <Typography
                    component='h1'
                    fontWeight={600}
                >
                    Педагогический состав
                </Typography>
            </Header>

            <SwitchButton
                showFirst={isLeftButtonSelected}
                handleClick={handleSwitch}
                leftButtonText="Спортивные инструкторы"
                rightButtonText="Руководители творческих студий"
                customStyles={switchButtonStyles}
                leftAltButtonText="Спортивные инструкторы"
                rightAltButtonText="Руководители творч. студий"
            />
            <TeachingStaffBlock>
                {content}
            </TeachingStaffBlock>
        </>

    )
}

const mapStateToProps = () => createStructuredSelector({
    teachingStaff: getOrgTeachingStaff
})

export default connect(mapStateToProps)(TeachingStaff)