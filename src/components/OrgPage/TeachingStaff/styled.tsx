import styled from 'styled-components'

const Wrapper = styled.div`
    display: -webkit-flex;
    display: flex;
    -webkit-flex-flow: column wrap;
    flex-flow: column wrap;
    align-content: center;
    justify-content: flex-start;
`

export const TeachingStaffBlock = styled(Wrapper)`
`

export const TeachingStaffPersonBlock = styled.div`
    display: flex;
    flex: 0 0 100%;

    padding: 16px 0;
    align-items: center;
    justify-content: center;
    

    @media (min-width: 810px) {
        flex: 0 0 45%;
        padding: 16px;
        justify-content: flex-start;
    }
`

export const switchButtonStyles = `
    display: flex;
    flex: 0 0 100%;

    padding: 0 0 16px;
    align-items: center;
    justify-content: center;
    

    @media (min-width: 810px) {
        flex: 0 0 45%;
        padding: 0 0 64px;
    }
`

export const Line = styled.div`
    width: auto;
    height: 1px;
    margin: 40px 0;
    background-color: rgba(0, 0, 0, 0.1);
`