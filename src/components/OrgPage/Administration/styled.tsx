import styled from 'styled-components'

const Wrapper = styled.div`
    display: -webkit-flex;
    display: flex;
    -webkit-flex-flow: row wrap;
    flex-flow: row wrap;
    align-content: center;
`

export const Header = styled(Wrapper)`
    margin-top: 64px;
    position: relative;
    justify-content: center;
    align-items: center;

    margin-bottom: 40px;

    @media (min-width: 810px) {
        margin-top: 100px;
    }

    h1 {
        flex: 1;
        font-size: 22px;
        text-align: center;

        @media (min-width: 810px) {
            font-size: 50px;
        }
    }

    h2 {
        margin-left: auto;

        font-size: 10px;
        font-weight: normal;
        line-height: 12px;


        @media (min-width: 810px) {
            position: absolute; 
            top: 0;
            right: 13px;

            font-size: 22px;
            font-weight: 600;
            line-height: 27px;

            padding: 33px;
        }
    }

    h2:hover {
        cursor: pointer;
        text-decoration: underline;
    }
`

export const AdministrationBlock = styled(Wrapper)`
`

export const AdministratorBlock = styled.div`
    display: flex;
    flex: 0 0 100%;

    padding: 16px 0;
    align-items: center;
    justify-content: center;
    font-family: Montserrat,sans-serif;
    

    @media (min-width: 810px) {
        flex: 0 0 45%;
        padding: 16px;
        justify-content: center;
    }
`
