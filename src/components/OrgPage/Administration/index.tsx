import React from 'react'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import { Typography, PersonVertical } from '@ijl/uds-ui'

import { getOrgAdministration, Administrators } from '@main/__data__/selectors/cur-org-selector'
import { Header, AdministratorBlock, AdministrationBlock } from './styled'


type AdministrationProps = {
    administrators: Administrators
}

const Administration: React.FC<AdministrationProps> = ({ administrators }) => {
    const content = administrators?.map((administrator) => (
        <AdministratorBlock key={administrator.name}>
            <PersonVertical
                name={administrator.name}
                jobTitle={administrator.post}
                gender={administrator.gender}
            />
        </AdministratorBlock>
    ))

    return (
        <>
            <Header>
                <Typography
                    component='h1'
                    fontWeight={600}
                >
                    Администрация
                </Typography>
            </Header>

            <AdministrationBlock>
                {content}
            </AdministrationBlock>
        </>
    )
}

const mapStateToProps = () => createStructuredSelector({
    administrators: getOrgAdministration
})

export default connect(mapStateToProps)(Administration)