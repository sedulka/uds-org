import styled from 'styled-components'

const Wrapper = styled.div`
    display: -webkit-flex;
    display: flex;
    -webkit-flex-flow: row wrap;
    flex-flow: row wrap;
`

export const SectionsBlock = styled(Wrapper)`

`

export const Header = styled(Wrapper)`
    margin-top: 64px;
    position: relative;
    justify-content: center;
    align-items: center;

    margin-bottom: 40px;

    @media (min-width: 810px) {
        margin-top: 100px;
    }

    h1 {
        flex: 0 0 100%;
        font-size: 22px;
        text-align: left;

        @media (min-width: 810px) {
            font-size: 50px;
            text-align: center;
        }
    }

    h2 {
        margin-left: auto;

        font-size: 10px;
        font-weight: normal;
        line-height: 12px;


        @media (min-width: 810px) {
            position: absolute; 
            top: 0;
            right: 13px;

            font-size: 22px;
            font-weight: 600;
            line-height: 27px;

            padding: 33px;
        }
    }

    h2:hover {
        cursor: pointer;
        text-decoration: underline;
    }

    img {
        position: relative;

        @media (min-width: 810px) {
            position: absolute; 
            top: 0;
            right: -9px;

            padding: 33px;
        }
    }
`

export const SectionsByAgeBlock = styled(Wrapper)`
    justify-content: space-between;
    flex: 0 0 100%;

    margin-bottom: 40px;

    max-height: ${props => props.isOpen ? '600px' : '0'};
    overflow: hidden;
    transition: max-height 0.3s ease-out;
    height: auto;
`

export const SectionBlock = styled.div`
    display: flex;
    flex: 0 0 100%;

    border-bottom: 1px solid rgba(0, 0, 0, 0.1);
    padding: 16px 0;

    cursor: pointer;
    align-items: center;

    button {
        display: none;
    }

    &:hover {
        background: rgba(0, 62, 255, 0.1);
    }

    @media (min-width: 810px) {
        flex: 0 0 45%;
        padding: 16px;

        button {
            display: block;
            visibility: hidden;
            margin-left: auto;
        }

        &:hover {
            button {
                visibility: visible;
            }
        }
    }

    
`

export const Ages = styled(Wrapper)`
    width: 100%;
    border-bottom: 1px solid rgba(0, 0, 0, 0.1);
    padding: 0;
    justify-content: space-between;
    align-items: center;

    &:hover {
        background: rgba(0, 62, 255, 0.1);
        cursor: pointer;
    }

    @media (min-width: 810px) {
        padding: 16px;
    }
`

export const LeftSection = styled(Wrapper)`
    flex: 1 0 45%;
`

export const RightSection = styled(Wrapper)`
    flex: 1 0 45%;
    justify-content: flex-end;
`

