import React from 'react';
import { Typography, Button } from '@ijl/uds-ui';

import { SectionBlock } from './styled'
import { StarIcon } from '@main/assets'

type SectionProps = {
    name: string;
    grade: number;
}

const Section: React.FC<SectionProps> = ({ name, grade }) => {
    let stars = [];
    for (let i = 0; i < grade; i++)
        stars.push(
            <img
                src={StarIcon}
                style={{ marginRight: '4px' }}
                key={i}
            />
        )

    return (
        <SectionBlock
            data-cy="section"
        >
            <Typography
                fontSize='16px'
                fontWeight={700}
                variant='primary'
                margin='0 8px 0 0'
            >
                {name}
            </Typography>
            {stars}

            <Button
                style={{ fontSize: '12px', fontWeight: 'bold' }}
            >
                Записаться
            </Button>
        </SectionBlock>
    )
}

export default Section