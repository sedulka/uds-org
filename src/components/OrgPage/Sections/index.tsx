import React from 'react'
import { connect } from 'react-redux'
import { Typography } from '@ijl/uds-ui'

import SectionsByAge from './SectionsByAge'
import { getOrgSections, Sections as SectionsInterface } from '@main/__data__/selectors/cur-org-selector'
import { SectionsBlock, Header } from './styled'
import { PDFIcon } from '@main/assets'
import { createStructuredSelector } from 'reselect'

type SectionsProps = {
    sections: SectionsInterface,
}

const Sections: React.FC<SectionsProps> = ({ sections }) => {
    const content = sections?.map((section) => (
        <SectionsByAge
            lower={section.range[0]}
            upper={section.range[1]}
            sections={section.sections}
            key={`${section.range[0]}-${section.range[1]}`}
        />
    ))

    return (
        <>
            <Header>
                <Typography
                    component='h1'
                    fontWeight={600}
                >
                    Секции
                </Typography>
                <Typography
                    component='h2'
                    fontWeight={600}
                    variant='primary'
                >
                    Расписание&nbsp;&nbsp;
                </Typography>
                <img src={PDFIcon} />
            </Header>

            <SectionsBlock>
                {content}
            </SectionsBlock>
        </>
    )
}

const mapStateToProps = () => createStructuredSelector({
    sections: getOrgSections
})

export default connect(mapStateToProps)(Sections)