import React, { useState } from 'react';
import { Typography } from '@ijl/uds-ui'

import Section from './Section'
import { SectionsByAgeBlock, Ages, LeftSection, RightSection } from './styled'
import { PlusIcon, MinusIcon } from '@main/assets'

type SectionsByAgeProps = {
    lower: number,
    upper: number,
    sections: {
        name: string,
        grade: number,
    }[]
}

const SectionsByAge: React.FC<SectionsByAgeProps> = ({ lower, upper, sections }) => {
    const [isOpen, setIsOpen] = useState(true)

    const handleClick = () => {
        setIsOpen(prev => !prev)
    }

    // Text for range
    const regularRangeText = `${lower}-${upper} лет`
    const olderRangeText = `${lower} лет и старше`

    // Sections
    const content = sections.map(section =>
        <Section name={section.name} grade={section.grade} key={section.name} />
    )

    return (
        <>
            <Ages
                onClick={handleClick}
                data-cy='section-button'
            >
                <LeftSection>
                    <Typography
                        fontSize='22px'
                        fontWeight={600}
                    >
                        {upper < 18 ? regularRangeText : olderRangeText}
                    </Typography>
                </LeftSection>
                <RightSection>
                    <img src={isOpen ? MinusIcon : PlusIcon} />
                </RightSection>
            </Ages>
            <SectionsByAgeBlock isOpen={isOpen}>
                {content}
            </SectionsByAgeBlock>
        </>
    )
}

export default SectionsByAge