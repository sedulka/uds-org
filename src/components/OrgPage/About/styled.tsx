import styled from 'styled-components'

const Wrapper = styled.div`
    display: -webkit-flex;
    display: flex;
    -webkit-flex-flow: column wrap;
    flex-flow: column wrap;
    align-content: center;
`

export const Header = styled(Wrapper)`
    margin-top: 64px;
    position: relative;
    justify-content: center;
    align-items: center;

    margin-bottom: 40px;

    @media (min-width: 810px) {
        margin-top: 100px;
    }

    h1 {
        flex: 1;
        font-size: 22px;
        text-align: center;

        @media (min-width: 810px) {
            font-size: 50px;
        }
    }

    h2 {
        margin-left: auto;

        font-size: 10px;
        font-weight: normal;
        line-height: 12px;


        @media (min-width: 810px) {
            position: absolute; 
            top: 0;
            right: 13px;

            font-size: 22px;
            font-weight: 600;
            line-height: 27px;

            padding: 33px;
        }
    }

    h2:hover {
        cursor: pointer;
        text-decoration: underline;
    }
`

export const DescriptionBlock = styled(Wrapper)`
    justify-content: center;
    
    @media (min-width: 810px) {
        padding: 0 100px;
        justify-content: center;
    }
`

export const VideoBlock = styled.div`
    display: flex;
    justify-content: center;
`

export const VideoFrame = styled.div`
    display: flex;
    justify-content: center;
    align-content: center;
    
    background: #003EFF;
    box-shadow: 0px 8px 32px rgba(24, 40, 151, 0.395126);
    border-radius: 4px;
    
    margin-bottom: 64px;
    
    @media (min-width: 810px) {
        width: 558px;
        height: 398px;
        margin-left: 200px;
        margin-right: 200px;
        
        box-shadow: 0px 16px 64px rgba(24, 40, 151, 0.395126);
        border-radius: 16px !important; 
    }
`

export const YouTubeVideo = styled.iframe`
    width: 100%;
    height: 100%;
    border-radius: 4px !important;
    
    @media (min-width: 810px) {
        border-radius: 16px !important; 
    }
`