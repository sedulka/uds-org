import React from 'react';
import {VideoFrame, VideoBlock, YouTubeVideo} from './styled'

type VideoProp = {
    videoURL: string;
}

const Video: React.FC<VideoProp> = ({ videoURL }) => {

    return (
        <VideoBlock>
            <VideoFrame>
                <YouTubeVideo
                    src={videoURL}
                    frameBorder="0"
                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                    allowFullScreen
                />
            </VideoFrame>
        </VideoBlock>
    )
}

export default Video