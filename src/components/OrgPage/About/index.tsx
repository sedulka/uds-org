import React from 'react'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import { Typography} from '@ijl/uds-ui'

import {getOrgAbout} from '@main/__data__/selectors/cur-org-selector'
import { Header, DescriptionBlock } from './styled'
import Video from "@main/components/OrgPage/About/Video";


type aboutOrgProp = {
    aboutOrg: {
        videoURL:string;
        description:string;
    }
}

const About: React.FC<aboutOrgProp> = ({ aboutOrg }) => {

    return (
        <>
            <Header>
                <Typography
                    component='h1'
                    fontWeight={600}
                >
                    Об организации
                </Typography>
            </Header>
            <Video videoURL={aboutOrg.videoURL}/>
            <DescriptionBlock>
                <Typography
                    fontWeight={400}
                    fontSize={"16px"}
                >
                    {aboutOrg.description}
                </Typography>
            </DescriptionBlock>
        </>
    )
}

const mapStateToProps = () => createStructuredSelector({
    aboutOrg: getOrgAbout
})

export default connect(mapStateToProps)(About)