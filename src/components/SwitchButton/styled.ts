import styled from 'styled-components'

export const ButtonsWrapper = styled.div`
    button {
        border: 1px solid #003EFF; /* Green border */
        padding: 10px 24px; /* Some padding */
        cursor: pointer; /* Pointer/hand icon */
        float: left; /* Float the buttons side by side */
        margin: ${props => props.margin ? props.margin : '0'};
    }

    button:not(:last-child) {
        border-right: none;
    }

    &:after {
        content: "";
        clear: both;
        display: table;
    }

    ${props => props.customStyles}
`

const Button = styled.button`
    font-family: Montserrat;
    font-style: normal;
    font-weight: bold;
    font-size: 12px;
    line-height: 15px;
`

const ButtonEnabled = styled(Button)`
    background: #003EFF !important;
    color: #FFFFFF;

    &: hover {
        background: #003EAF !important;
    }
    
    &:before {
        content: "${props => props.alt || props.text}";
    }
    
    @media (min-width: 810px) {
        &:before {
            content: "${props => props.text}";
        }
    } 
`

const ButtonDisabled = styled(Button)`
    background: #FFFFFF !important;
    border: 1px solid #003EFF;
    color:  #003EFF;

    &: hover {
        background: #F5F5F5 !important;
    }
    
    &:before {
        content: "${props => props.alt || props.text}";
    }
    
    @media (min-width: 810px) {
        &:before {
            content: "${props => props.text}";
        }
    } 
`

const leftRadius = "8px 0px 0px 8px"
const rightRadius = "0px 8px 8px 0px"

export const LeftButtonEnabled = styled(ButtonEnabled)`
    border-radius: ${leftRadius};
`

export const LeftButtonDisabled = styled(ButtonDisabled)`
    border-radius: ${leftRadius};
`

export const RightButtonEnabled = styled(ButtonEnabled)`
    border-radius: ${rightRadius};
`

export const RightButtonDisabled = styled(ButtonDisabled)`
    border-radius: ${rightRadius};
`