import React from 'react'

import {
    LeftButtonDisabled,
    LeftButtonEnabled,
    RightButtonDisabled,
    RightButtonEnabled,
    ButtonsWrapper
} from './styled'

interface OnClickType {
    (e: React.HTMLProps<React.MouseEvent>): void;
}

/**
 * @prop {string} leftButtonText Text of the left button
 * @prop {string} rightButtonText Text of the right button
 * @prop {boolean} showFirst Determine which button should be active
 * @prop {OnClickType} handleClick Method that will be called after click to any button
 * @prop {string} margin The margin around the switch button. Optional parameter
 * @prop {string} customStyles Any additional styles. Optional parameter
 */
interface Props {
    leftButtonText: string;
    rightButtonText: string;
    leftAltButtonText: string;
    rightAltButtonText: string;
    showFirst: boolean;
    handleClick: OnClickType;
    margin?: string;
    customStyles?: string;
}

const SwitchButton: React.FC<Props> = ({showFirst, handleClick, leftButtonText, rightButtonText, leftAltButtonText, rightAltButtonText, margin = "", customStyles}) => (
    <ButtonsWrapper margin={margin} customStyles={customStyles} data-cy='switch-button'>
        {showFirst
            ?
            <>
                <LeftButtonEnabled text={leftButtonText} alt={leftAltButtonText} onClick={handleClick}
                                   data-cy='switch-button-left'/>
                <RightButtonDisabled text={rightButtonText} alt={rightAltButtonText} onClick={handleClick}
                                     data-cy='switch-button-right'/>
            </>
            :
            <>
                <LeftButtonDisabled text={leftButtonText} alt={leftAltButtonText} onClick={handleClick}
                                    data-cy='switch-button-left'/>
                <RightButtonEnabled text={rightButtonText} alt={rightAltButtonText} onClick={handleClick}
                                    data-cy='switch-button-right'/>
            </>
        }
    </ButtonsWrapper>
)

export default SwitchButton