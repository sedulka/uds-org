import React from 'react'
import { Footer } from '@ijl/uds-ui'

export default function index() {
    return (
        <div data-cy='footer'>
            <Footer />
        </div>
    )
}
