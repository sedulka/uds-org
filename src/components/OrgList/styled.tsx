import styled from 'styled-components'
import { device } from '@main/__data__/constants'

const showLine = (toShow) => {
    return toShow ? '1px solid rgba(0, 0, 0, 0.1)' : 'none'
}

export const DistrictsWrapper = styled.div`
    display: -webkit-flex;
    display: flex;
    -webkit-flex-flow: row wrap;
    flex-flow: row wrap;
`

export const DistrictBlock = styled.div`
    border-bottom: 1px solid rgba(0, 0, 0, 0.1);
    margin-bottom: 40px;
    padding-bottom: 40px;
    flex: 1 0 100%;
    border-bottom: ${props => showLine(!props.removeFromMobileL)};

    @media ${device.tabletS} {
        flex: 1 0 50%;
        border-bottom: ${props => showLine(!props.removeFromTabletS)};
    };

    @media ${device.tabletL} {
        flex: 1 0 33%;
        border-bottom: ${props => showLine(!props.removeFromTabletL)};
    };

    @media ${device.laptop} {
        flex: 0 0 25%;
        border-bottom: ${props => showLine(!props.removeFromLaptop)};
    };
`

export const OrgsBlock = styled.div`
    max-height: ${props => props.isOpen ? '600px' : '0'};
    overflow: hidden;
    transition: max-height 0.3s ease-out;
    height: auto;
`