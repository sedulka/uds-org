import React, { Component } from 'react';
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect';

import { getDistricts } from "@main/__data__/selectors/orgs-selector";
import { DistrictsWrapper } from './styled'
import District from './District';

const OrgList = ({ districts }) => {
    const isLineHidden = (index: number, num: number) => {
        // Is number of districts divisible by num
        const isDivisible = districts?.length % num === 0

        // If isDisivible get last `num` districts
        const expr1 = isDivisible && (districts?.length / num - 1) * num <= index

        // Otherwise take just last
        const expr2 = index >= Math.floor(districts?.length / num) * num
        return expr1 || expr2
    }

    const districtsJSX = districts?.map((dist, index) => {
        const lineVisibilities = [
            index === districts?.length - 1,
            isLineHidden(index, 2),
            isLineHidden(index, 3),
            isLineHidden(index, 4)
        ]

        return (
            <District name={dist} key={dist} lineVisibilities={lineVisibilities} />
        )
    });

    return (
        <DistrictsWrapper>
            {districtsJSX}
        </DistrictsWrapper>
    )
}

const mapStateToProps = () => createStructuredSelector({
    districts: getDistricts,
})


export default connect(mapStateToProps)(OrgList)