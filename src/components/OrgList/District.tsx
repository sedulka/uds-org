import React from 'react';
import { connect } from 'react-redux'
import { Typography } from '@ijl/uds-ui'

import Area from './Area';
import { getAreas } from "@main/__data__/selectors/orgs-selector";
import { DistrictBlock } from './styled'

type DistrictProps = {
    name: any,
    areas: string[],
    removeFromMobileL: number,
    removeFromTabletS: number,
    removeFromTabletL: number,
    removeFromLaptop: number,
}

const District: React.FC<DistrictProps> = ({ name, areas, removeFromMobileL, removeFromLaptop, removeFromTabletL, removeFromTabletS }) => {
    const areasJSX = areas?.map(area => <Area name={area} key={area} />)

    return (
        <DistrictBlock
            removeFromMobileL={removeFromMobileL}
            removeFromTabletS={removeFromTabletS}
            removeFromTabletL={removeFromTabletL}
            removeFromLaptop={removeFromLaptop}
        >
            <Typography
                component={'h2'}
                fontSize="22px"
                fontWeight={600}
                margin="0 0 8px 0"
            >
                {name}
            </Typography>
            {areasJSX}
        </DistrictBlock>
    )
}

const mapStateToProps = (state, ownProps) => {
    return {
        areas: getAreas(state)[ownProps.name],
        name: ownProps.name,
        removeFromMobileL: ownProps.lineVisibilities[0],
        removeFromTabletS: ownProps.lineVisibilities[1],
        removeFromTabletL: ownProps.lineVisibilities[2],
        removeFromLaptop: ownProps.lineVisibilities[3],
    }
}

export default connect(mapStateToProps)(District)