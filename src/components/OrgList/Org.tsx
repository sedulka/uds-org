import React from 'react';
import { Typography } from '@ijl/uds-ui'
import { Link } from 'react-router-dom'

type OrgProps = {
    name: string,
}

const Org: React.FC<OrgProps> = ({ name }) => {
    const hoverStyle = `
        &: hover {
            text-decoration: underline;
        }
    `

    return (
        <Link to='/orgs/1' style={{ textDecoration: 'none' }}>
            <div
                style={{ cursor: "pointer", paddingLeft: "10px" }}
            >
                <Typography
                    component={'h1'}
                    fontSize="16px"
                    fontWeight={400}
                    margin="10px 0 10px 0"
                    variant="primary"
                    customStyle={hoverStyle}
                >
                    {name}
                </Typography>
            </div>
        </Link>
    )
}

export default Org