import React, { useState } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { Typography } from '@ijl/uds-ui'

import { getOrgs } from "@main/__data__/selectors/orgs-selector";
import { OrgsBlock } from './styled'
import Org from './Org';

type AreaProps = {
    name: string,
    orgs: Array<string>,
}

const Area: React.FC<AreaProps> = ({ name, orgs }) => {
    const [isOpen, setIsOpen] = useState(false)

    const isActive = orgs.length > 0 ? true : false

    const handleClick = () => {
        if (isActive) {
            setIsOpen(prev => !prev);
        }
    }

    const orgsJSX = orgs.map(org => <Org name={org} key={org} />)

    const hoverStyle = `
        &: hover {
            text-decoration: underline;
        }

        transition: margin 0.5s ease-out;
    `

    return (
        <div>
            <Typography
                component={'h2'}
                fontSize="16px"
                fontWeight={400}
                margin={`0 0 ${isOpen ? '0' : '8px'} 0`}
                opacity={isActive ? "1" : "0.3"}
                customStyle={hoverStyle}
            >
                <span
                    style={{ cursor: isActive ? "pointer" : "default" }}
                    onClick={handleClick}
                    data-cy='area'
                >
                    {name}
                </span>
            </Typography>

            <OrgsBlock isOpen={isOpen} data-cy='orgs-block'>
                {orgsJSX}
            </OrgsBlock>
        </div>
    )
}

const mapStateToProps = (state, props) => {
    return {
        name: props.name,
        orgs: getOrgs(state)[props.name],
    }
}

export default connect(mapStateToProps)(Area)
