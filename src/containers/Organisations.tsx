import React, { useState, useEffect } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect';
import { getNavigations } from '@ijl/cli'
import { Typography, PageLoader, Error } from '@ijl/uds-ui'

import OrgList from '@main/components/OrgList';
import OrgMap from '@main/components/OrgMap';
import SwitchButton from '@main/components/SwitchButton'
import Navbar from '@main/components/Navbar'
import Footer from '@main/components/Footer'
import fetchOrgsData from '@main/__data__/actions/orgs'
import { getOrgsDataLoading, getDistricts, getOrgsDataError } from '@main/__data__/selectors/orgs-selector'
import { Wrapper, titleStyles, switchButtonStyles } from './styled'

const Organisations = ({ fetchOrgsData, isLoading, districts, isError }) => {
    useEffect(() => { fetchOrgsData() }, [])

    const [isList, setIsList] = useState(true)
    const handleSwitch = () => setIsList(prev => !prev)

    if (isError) {
        return <Error type='error' link={getNavigations()['main']} linkText='Перейти на главную' />
    }

    if (isLoading || !districts) {
        return <PageLoader />
    }

    //  Take the content of the page based on isList variable
    let content: JSX.Element;
    if (isList) {
        content = <OrgList />;
    } else {
        content = <OrgMap />;
    }

    return (
        <>
            <Wrapper>
                <Navbar />
                <Typography
                    component={'h1'}
                    fontWeight={600}
                    customStyle={titleStyles}
                >
                    Учреждения досуга и спорта в Москве
                </Typography>

                <SwitchButton
                    showFirst={isList}
                    handleClick={handleSwitch}
                    leftButtonText="Списком"
                    rightButtonText="На Карте"
                    leftAltButtonText="Списком"
                    rightAltButtonText="На Карте"
                    customStyles={switchButtonStyles}
                />
                {content}
            </Wrapper >
            <Footer />
        </>

    )
}

const mapStateToProps = (state) => createStructuredSelector({
    isError: getOrgsDataError,
    isLoading: getOrgsDataLoading,
    districts: getDistricts,
})

const mapDispatchToProps = (dispatch: any) => bindActionCreators({
    fetchOrgsData
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Organisations)