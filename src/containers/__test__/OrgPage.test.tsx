import React from 'react'
import { mount, configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import * as Thunk from 'redux-thunk'
import moxios from 'moxios'
import { act } from 'react-dom/test-utils'
import { Router } from 'react-router-dom'
import { createBrowserHistory } from 'history'

import reducers from '../../__data__/redusers'

import OrgPage from '../OrgPage'

configure({ adapter: new Adapter() })

const history = createBrowserHistory()

describe('mount organisation page', () => {
    beforeEach(() => {
        moxios.install()
        //@ts-ignore
        window.__webpack_public_path__ = '/'
    })

    afterEach(() => {
        moxios.uninstall()
    })

    it('mount', async () => {
        const app = mount(
            <Provider store={createStore(reducers, applyMiddleware(Thunk.default))}>
                <Router history={history}>
                    <OrgPage />
                </Router>
            </Provider>
        )

        expect(app).toMatchSnapshot()

        await moxios.wait(jest.fn)

        await act(async () => {
            const request = moxios.requests.mostRecent()
            await request.respondWith({
                status: 200,
                response: require('../../../stubs/api/org.data.json')
            })
        })

        app.update()
        expect(app).toMatchSnapshot()
    })
    it('mount with error', async () => {
        const app = mount(
            <Provider store={createStore(reducers, applyMiddleware(Thunk.default))}>
                <Router history={history}>
                    <OrgPage />
                </Router>
            </Provider>
        )

        expect(app).toMatchSnapshot()

        await moxios.wait(jest.fn)

        await act(async () => {
            const request = moxios.requests.mostRecent()
            await request.respondWith({
                status: 404,
            })
        })

        app.update()
        expect(app).toMatchSnapshot()
    })

})

