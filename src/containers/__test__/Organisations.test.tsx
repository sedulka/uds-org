import React from 'react'
import { mount, configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import * as Thunk from 'redux-thunk'
import moxios from 'moxios'
import { act } from 'react-dom/test-utils'
import { Router } from 'react-router-dom'
import { createBrowserHistory } from 'history'
import GoogleMapReact from 'google-map-react';

import reducers from '../../__data__/redusers'

import Organisations from '../Organisations'

const history = createBrowserHistory()

configure({ adapter: new Adapter() })

describe('mount organisations page', () => {
    beforeEach(() => {
        moxios.install()
        //@ts-ignore
        window.__webpack_public_path__ = '/'
    })

    afterEach(() => {
        moxios.uninstall()
    })

    it('mount organisations list', async () => {
        const app = mount(
            <Provider store={createStore(reducers, applyMiddleware(Thunk.default))}>
                <Router history={history}>
                    <Organisations />
                </Router>
            </Provider>
        )

        expect(app).toMatchSnapshot()

        await moxios.wait(jest.fn)

        await act(async () => {
            const request = moxios.requests.mostRecent()
            await request.respondWith({
                status: 200,
                response: require('../../../stubs/api/orgs.data.json')
            })
        })
        app.update()
        expect(app).toMatchSnapshot()
    })

    it('mount error on error', async () => {
        const app = mount(
            <Provider store={createStore(reducers, applyMiddleware(Thunk.default))}>
                <Router history={history}>
                    <Organisations />
                </Router>
            </Provider>
        )

        expect(app).toMatchSnapshot()

        await moxios.wait(jest.fn)

        await act(async () => {
            const request = moxios.requests.mostRecent()
            await request.respondWith({
                status: 404,
            })
        })

        app.update()
        expect(app).toMatchSnapshot()
    })


    it('mount organistaions map', async () => {
        const app = mount(
            <Provider store={createStore(reducers, applyMiddleware(Thunk.default))}>
                <Router history={history}>
                    <Organisations />
                </Router>
            </Provider>
        )

        expect(app).toMatchSnapshot()

        await moxios.wait(jest.fn)

        await act(async () => {
            const request = moxios.requests.mostRecent()
            await request.respondWith({
                status: 200,
                response: require('../../../stubs/api/orgs.data.json')
            })
        })

        app.update()

        app.find('button').at(0).simulate('click')

        expect(app.find(GoogleMapReact).length).toBe(1)
    })
})
