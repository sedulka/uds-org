import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { useParams } from 'react-router-dom'
import { createStructuredSelector } from 'reselect'
import { OrganizationInfo, PageLoader, ReachGoals, Error, SocialShare } from '@ijl/uds-ui'
import { getNavigations } from '@ijl/cli'

import { Sections, Administration } from '@main/components/OrgPage'
import Navbar from '@main/components/Navbar'
import Footer from '@main/components/Footer'
import { Wrapper, ReachGoalsWrapper, SocialShareWrapper } from './styled'
import fetchOrganisationData from '@main/__data__/actions/organisation'
import { getOrgName, getOrgDescription, getOrgAddress, getOrgDataLoading, getOrgDataError, getOrgLinks } from '@main/__data__/selectors/cur-org-selector'
import About from "@main/components/OrgPage/About";
import Awards from "@main/components/OrgPage/Awards";
import {LogoIcon} from '@main/assets'
import TeachingStaff from "@main/components/OrgPage/TeachingStaff";

type OrgPageProps = {
    fetchOrganisationData: Function;
    name: string;
    description: string;
    address: string;
    isLoading: boolean;
    isError: boolean;
    links: {
        vk: string;
        instagram: string;
        facebook: string;
        ok: string;
        twitter: string;
    };
}

const OrgPage: React.FC<OrgPageProps> = ({ fetchOrganisationData, name, description, address, isLoading, isError, links }) => {
    const { id } = useParams();

    useEffect(() => {
        fetchOrganisationData(id)
    }, [])

    if (isError) {
        return <Error type='error' link={getNavigations()['main']} linkText='Перейти на главную' />
    }

    if (isLoading || !name || !description || !address || !links) {
        return <PageLoader />
    }

    return (
        <>
            <Wrapper>
                <Navbar />
                <OrganizationInfo
                    title={name}
                    mainText={description}
                    bottomText={address}
                    buttonText="Записаться"
                    src={LogoIcon}
                />
                <Sections />
                <Administration />
                <About />
                <Awards />
                <Administration/>
                <TeachingStaff/>

            </Wrapper>

            <SocialShareWrapper>
                <SocialShare
                    title="Мы в социальных сетях"
                    alt="Мы в соцсетях"
                    vk_link={links?.vk}
                    instagram_link={links?.instagram}
                    facebook_link={links?.facebook}
                    ok_link={links?.ok}
                    twitter_link={links?.twitter}
                />
            </SocialShareWrapper>

            <ReachGoalsWrapper>
                <ReachGoals
                    title="Достигайте своих целей"
                    buttonText="Записаться"
                />
            </ReachGoalsWrapper>
            <Footer />
        </>
    )
}

const mapStateToProps = (state) => createStructuredSelector({
    name: getOrgName,
    description: getOrgDescription,
    address: getOrgAddress,
    links: getOrgLinks,
    isLoading: getOrgDataLoading,
    isError: getOrgDataError,
})

const mapDispatchToProps = (dispatch: any) => bindActionCreators({
    fetchOrganisationData
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(OrgPage)