import React from 'react';
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { Route, Switch } from 'react-router'
import { Router } from 'react-router-dom'
import { composeWithDevTools } from 'redux-devtools-extension';
import { createBrowserHistory } from 'history'
import { getNavigations } from '@ijl/cli'

import Organisations from '@main/containers/Organisations';
import OrgPage from '@main/containers/OrgPage'
import reducers from '@main/__data__/redusers'

const history = createBrowserHistory();

export default () => (
    <Provider store={createStore(reducers, composeWithDevTools(applyMiddleware(thunk)))}>
        <Router history={history}>
            <Switch>
                <Route path={getNavigations()['orgs']} component={Organisations} exact />
                <Route path={getNavigations()['orgs'] + '/:id'} component={OrgPage} />
            </Switch>
        </Router>
    </Provider>
)